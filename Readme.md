# helm-project #

Support for projects in Emacs, relying on [Helm][1] for choice/completion.

[1]: https://emacs-helm.github.io/helm "Emacs incremental completion and selection narrowing framework"
