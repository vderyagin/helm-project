(require 'path-trie)
(require 'subr-x)

(defmacro with-empty-trie (&rest body)
  `(thread-first nil ,@body))

(describe "path-trie-split-path"
  (it "returns a singleton list for simple path"
    (expect (path-trie-split-path "foo")
            :to-equal
            '(foo)))

  (it "breaks up long paths"
    (expect (path-trie-split-path "/foo/bar/baz/quux/")
            :to-equal
            '(foo bar baz quux)))

  (it "does not care about leading/trailing slashes"
    (expect (path-trie-split-path "foo/bar/baz/quux")
            :to-equal
            (path-trie-split-path "foo/bar/baz/quux"))))

(describe "inserting/retrieving objects"
  (it "returns `nil' when nothing is there"
    (expect (path-trie-get nil "/non/existent/path") :to-be nil))

  (it "rejects item that are lists"
    (expect (path-trie-put nil "/foo/baz" nil)
            :to-throw 'error)
    (expect (path-trie-put nil "/foo/baz" (list :foo :bar))
            :to-throw 'error))

  (it "rejects relative-paths"
    (expect (path-trie-put nil "bar/baz" :obj)
            :to-throw 'error))

  (it "allows to store and retrieve an object"
    (expect (with-empty-trie
             (path-trie-put "/foo/bar" :obj)
             (path-trie-get "/foo/bar"))
            :to-be
            :obj))

  (it "allows to store and retrieve multiple objects"
    (let ((trie (with-empty-trie
                 (path-trie-put "/foo/bar/quux" :a)
                 (path-trie-put "/x/y/z" :b))))
      (expect (path-trie-get trie "/foo/bar/quux") :to-be :a)
      (expect (path-trie-get trie "/x/y/z") :to-be :b)))

  (it "does not care about trailing slash"
    (expect (with-empty-trie
             (path-trie-put "/foo/bar/baz/" :obj)
             (path-trie-get "/foo/bar/baz"))
            :to-be
            :obj))

  (it "returns first encountered value when dealing with deep queries"
    (expect (with-empty-trie
             (path-trie-put "/foo/bar" :obj)
             (path-trie-get "/foo/bar/baz/quux/corge/grault"))
            :to-be
            :obj))

  (it "overwrites children when assigning parent"
    (let ((trie (with-empty-trie
                 (path-trie-put "/foo/bar/baz" :a)
                 (path-trie-put "/foo/bar/quux" :b)
                 (path-trie-put "/foo/bar" :obj))))
      (expect (path-trie-get trie "/foo/bar/baz") :to-be :obj)
      (expect (path-trie-get trie "/foo/bar/quux") :to-be :obj)
      (expect (path-trie-get trie "/foo/bar") :to-be :obj)))

  (it "overwrites parent values when assigning a deeper child path"
    (let ((trie (with-empty-trie
                 (path-trie-put "/foo/bar" :initial)
                 (path-trie-put "/foo/bar/baz" :overwritten))))
      (expect (path-trie-get trie "/foo/bar") :to-be nil)
      (expect (path-trie-get trie "/foo/bar/baz") :to-be :overwritten))))

(describe "removing items"
  (it "removes existing items"
    (let ((trie (with-empty-trie
                 (path-trie-put "/foo/bar" :a)
                 (path-trie-put "/foo/quux" :b)
                 (path-trie-remove "/foo/bar"))))
      (expect (path-trie-get trie "/foo/bar") :to-be nil)
      (expect (path-trie-get trie "/foo/quux") :to-be :b)))

  (it "does not make a mess when asked to remove nonexistent items"
    (expect (with-empty-trie
             (path-trie-remove "/foo")
             (path-trie-get "/foo"))
            :to-be
            nil))

  (it "does not screw up parent value when asked to remove nonexistent child"
    (expect (with-empty-trie
             (path-trie-put "/foo/bar" :obj)
             (path-trie-remove "/foo/bar/baz")
             (path-trie-get "/foo/bar"))
            :to-be
            :obj)))

(describe "path-trie-reduce"
  (it "allows to fold over values in trie"
    (let ((trie (with-empty-trie
                 (path-trie-put "/foo" 1)
                 (path-trie-put "/bar/baz" 2)
                 (path-trie-put "/bar/quux" 3)
                 (path-trie-put "/a/b/c/c/e/z" 4))))
      (expect (path-trie-reduce #'+ trie 0)
              :to-be
              (+ 1 2 3 4)))))

(describe "path-trie-size"
  (it "says that size of empty trie is zero"
    (expect (with-empty-trie (path-trie-size)) :to-be 0))

  (it "calculates size of non-empty tries"
    (expect (with-empty-trie
             (path-trie-put "/foo/bar" :obj)
             (path-trie-put "/foo/quux" :obj)
             (path-trie-put "/boo/bar" :obj)
             (path-trie-put "/z/b/c/x" :obj)
             path-trie-size)
            :to-be
            4))

  (it "is not confused by overwriting/removing items"
    (expect (with-empty-trie
             (path-trie-put "/foo/bar" :obj)
             (path-trie-put "/foo/quux" :obj)
             (path-trie-put "/foo/bar/baz" :obj)
             (path-trie-put "/foo" :obj)
             (path-trie-put "/z/b/c/x" :obj)
             (path-trie-remove "/z/b/c/x")
             path-trie-size)
            :to-be
            1)))

(describe "path-trie-values"
  (it "retrieves list of values from a trie"
    (expect (with-empty-trie
             (path-trie-put "/foo/bar" 1)
             (path-trie-put "/foo/baz" 2)
             (path-trie-put "/quux/corge" 3)
             (path-trie-put "/foo/grault" 4)
             (path-trie-put "/a/b/z" 5)
             path-trie-values)
            :to-have-same-items-as
            (list 1 2 3 4 5))))
