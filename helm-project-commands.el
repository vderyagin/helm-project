;;; -*- lexical-binding: t -*-

(require 'subr-x)
(require 'helm)
(require 'map)

(defvar helm-project-commands-cache (make-hash-table :test #'equal))

(defvar helm-project-commands-test nil)
(defvar helm-project-commands-build nil)

(defclass helm-project-commands-cmd ()
  ((name
    :initarg :name
    :type string))
  :abstract t)

(cl-defmethod helm-project-commands-cmd-candidate ((cmd helm-project-commands-cmd))
  (cons (oref cmd name) cmd))

(cl-defmethod helm-project-commands-cmd-build-p ((cmd helm-project-commands-cmd))
  (string= (oref cmd name) "build"))

(cl-defmethod helm-project-commands-cmd-test-p ((cmd helm-project-commands-cmd))
  (string= (oref cmd name) "run tests"))

(defclass helm-project-commands-shell-cmd (helm-project-commands-cmd)
  ((command
    :initarg :command
    :type string)))

(cl-defmethod helm-project-commands-cmd-run ((cmd helm-project-commands-shell-cmd) dir)
  (let ((default-directory dir))
    (compile (oref cmd command))))

(cl-defmethod helm-project-commands-cmd-edit-and-run ((cmd helm-project-commands-shell-cmd) dir)
  (let ((default-directory dir))
    (thread-first "Command to execute: "
      (read-string (oref cmd command))
      compile)))

(defclass helm-project-commands-function-cmd (helm-project-commands-cmd)
  ((command
    :initarg :command
    :type function)))

(cl-defmethod helm-project-commands-cmd-run ((cmd helm-project-commands-function-cmd) dir)
  (let ((default-directory dir))
    (funcall (oref cmd command))))

(cl-defmethod helm-project-commands-cmd-edit-and-run ((_ helm-project-commands-function-cmd))
  (error "Not implemented"))

(defun helm-project-commands-cmd-build (name body)
  (let ((class-fn (pcase body
                    ((pred stringp) #'helm-project-commands-shell-cmd)
                    ((pred functionp) #'helm-project-commands-function-cmd)
                    (_ (error "Command must be either string or function"))))
        (proper-name (pcase name
                       (:build "build")
                       (:test  "run tests")
                       (_      name))))
    (funcall class-fn :name proper-name :command body)))

(defun helm-project-commands-test-cmd ()
  (when (and (boundp 'helm-project-commands-test)
             (stringp helm-project-commands-test))
    (helm-project-commands-cmd-build :test helm-project-commands-test)))

(defun helm-project-commands-build-cmd ()
  (when (and (boundp 'helm-project-commands-build)
             (stringp helm-project-commands-build))
    (helm-project-commands-cmd-build :build helm-project-commands-build)))

(defclass helm-project-commands-set ()
  ((name
    :initarg :name
    :type string)
   (commands
    :initarg :commands
    ;; :type helm-project-commands-cmd-list
    :reader helm-project-commands-set-commands)
   (find-root
    :initarg :find-root
    :type function)))

(cl-defmethod helm-project-commands-set-build-cmd ((set helm-project-commands-set))
  (thread-last set
    helm-project-commands-set-commands
    (seq-find #'helm-project-commands-cmd-build-p)))

(cl-defmethod helm-project-commands-set-test-cmd ((set helm-project-commands-set))
  (thread-last set
    helm-project-commands-set-commands
    (seq-find #'helm-project-commands-cmd-test-p)))

(cl-defmethod helm-project-commands-set-source ((set helm-project-commands-set) root)
  (with-slots (name commands) set
    (helm-build-sync-source name
      :candidates
      (seq-map #'helm-project-commands-cmd-candidate commands)
      :nomark
      t
      :keymap
      (let* ((applicable-sets (thread-last helm-project-commands-cache
                                (map-values-apply (lambda (set)
                                                    (when-let (root (funcall (oref set find-root)))
                                                      (cons root set))))
                                (seq-filter #'identity)))
             (build-cmd (or (when-let (cmd (helm-project-commands-build-cmd))
                              (cons (locate-dominating-file default-directory ".git") cmd))
                            (thread-last applicable-sets
                              (seq-map (pcase-lambda (`(,root . ,set))
                                         (when-let (build (helm-project-commands-set-build-cmd set))
                                           (cons root build))))
                              (seq-find #'identity))))
             (test-cmd (or (when-let (cmd (helm-project-commands-test-cmd))
                             (cons (locate-dominating-file default-directory ".git") cmd))
                           (thread-last applicable-sets
                             (seq-map (pcase-lambda (`(,root . ,set))
                                        (when-let (build (helm-project-commands-set-test-cmd set))
                                          (cons root build))))
                             (seq-find #'identity))))
             (map (make-sparse-keymap)))
        (set-keymap-parent map helm-map)
        (define-key map (kbd "C-c C-c")
          (lambda ()
            (interactive)
            (if build-cmd
                (helm-run-after-exit
                 #'helm-project-commands-cmd-run
                 (cdr build-cmd)
                 (car build-cmd))
              (message "No build command found"))))
        (define-key map (kbd "C-c C-t")
          (lambda ()
            (interactive)
            (if test-cmd
                (helm-run-after-exit
                 #'helm-project-commands-cmd-run
                 (cdr test-cmd)
                 (car test-cmd))
              (message "No test command found"))))
        map)
      :action
      (list
       (cons "Run"
             (lambda (cmd)
               (helm-project-commands-cmd-run cmd root)))
       (cons "Edit & run"
             (lambda (cmd)
               (helm-project-commands-cmd-edit-and-run cmd root))))
      :action-transformer
      (lambda (actions cmd)
        (seq-filter
         (pcase-lambda (`(,name . _))
           (if (string= name "Edit & run")
               (helm-project-commands-shell-cmd-p cmd)
             t))
         actions))
      :mode-line
      (lambda ()
        (let ((candidate (car (helm-marked-candidates)))) ; there's only one, no marking allowed
          (list "Command(s)"
                (mapconcat #'identity
                           (list "RET:Run"
                                 (when (helm-project-commands-shell-cmd-p candidate)
                                   "f2:Edit & Run")
                                 "C-c C-c:Build"
                                 "C-c C-t:Run tests")
                           " ")))))))

(defun helm-project-commands-run-command ()
  (let ((sources (thread-last helm-project-commands-cache
                   (map-values-apply (lambda (set)
                                       (when-let (root (funcall (oref set find-root)))
                                         (cons root set))))
                   (seq-filter #'identity)
                   (seq-map (pcase-lambda (`(,root . ,set))
                              (helm-project-commands-set-source set root))))))
    (unless sources (error "No applicable commands found"))
    (helm
     :buffer "*helm project commands*"
     :prompt "Command: "
     :sources sources)))

(defun helm-project-commands-set-test-command (cmd)
  (add-dir-local-variable nil 'helm-project-commands-test cmd))

(defun helm-project-commands-set-build-command (cmd)
  (add-dir-local-variable nil 'helm-project-commands-build cmd))

(defmacro helm-project-commands-set-build (name find-root-fn commands)
  (declare (indent 1))
  `(map-put
    helm-project-commands-cache
    ,name
    (helm-project-commands-set
     :name ,name
     :find-root ,find-root-fn
     :commands (seq-map
                (pcase-lambda (`(,cmd-name . ,cmd-body))
                  (helm-project-commands-cmd-build cmd-name cmd-body))
                ,commands))))

(defmacro helm-project-commands-add (set-name cmd-name cmd-body)
  (declare (indent 1))
  `(let ((cmd (helm-project-commands-cmd-build ,cmd-name ,cmd-body))
         (set (map-elt helm-project-commands-cache ,set-name)))
     (oset set commands (cons cmd (oref set commands)))))

(helm-project-commands-set-build "Cask"
  (lambda ()
    (when-let (root (locate-dominating-file default-directory "Cask"))
      (expand-file-name root)))
  '((:build                 . "cask build")
    ("install depencencies" . "cask install")
    ("update dependencies"  . "cask update")
    ("cleanup bytecode"     . "cask clean-elc")
    ("list dependencies"    . "cask list")
    ("eval expression"      . (lambda ()
                                (thread-last "Expression: "
                                  read--expression
                                  prin1-to-string
                                  (format "cask eval '(message \"%%s\" %s)'")
                                  compile)))))

(helm-project-commands-set-build "Cargo"
  (lambda ()
    (when-let (root (locate-dominating-file default-directory "Cargo.toml"))
      (expand-file-name root)))
  '((:build                      . "cargo build")
    (:test                       . "cargo test")
    ("release build"             . "cargo build --release")
    ("run benchmarks"            . "cargo bench")
    ("update dependencies"       . "cargo update")
    ("run"                       . "cargo run")
    ("check & report errors"     . "cargo check")
    ("remove 'target' directory" . "cargo clean")))

(when (executable-find "cargo-outdated")
  (helm-project-commands-add "Cargo"
    "list outdated dependencies"
    "cargo outdated"))

(when (executable-find "cargo-list")
  (helm-project-commands-add "Cargo"
    "list dependencies"
    "cargo list"))

(when (executable-find "cargo-rm")
  (helm-project-commands-add "Cargo"
    "remove dependency"
    (lambda ()
      (thread-last "Crate: "
        read-string
        (format "cargo rm %s")
        compile)))

  (helm-project-commands-add "Cargo"
    "remove development dependency"
    (lambda ()
      (thread-last "Crate: "
        read-string
        (format "cargo rm --build %s")
        compile)))

  (helm-project-commands-add "Cargo"
    "remove build dependency"
    (lambda ()
      (thread-last "Crate: "
        read-string
        (format "cargo rm --build %s")
        compile))))

(when (executable-find "cargo-add")
  (helm-project-commands-add "Cargo"
    "add dependency"
    (lambda ()
      (thread-last "Crate: "
        read-string
        (format "cargo add %s")
        compile)))

  (helm-project-commands-add "Cargo"
    "add development dependency"
    (lambda ()
      (thread-last "Crate: "
        read-string
        (format "cargo add --dev %s")
        compile)))

  (helm-project-commands-add "Cargo"
    "add build dependency"
    (lambda ()
      (thread-last "Crate: "
        read-string
        (format "cargo add --build %s")
        compile))))

(when (executable-find "cargo-fmt")
  (helm-project-commands-add "Cargo"
    "format code"
    "cargo fmt"))

(helm-project-commands-set-build "Bundle"
  (lambda ()
    (when-let (root (locate-dominating-file default-directory "Gemfile"))
      (expand-file-name root)))
  '(("install dependencies"       . "bundle install")
    ("update dependencies"        . "bundle update")
    ("list outdated dependencies" . "bundle outdated")))

(helm-project-commands-set-build "Ruby on Rails"
  (lambda ()
    (when-let (root
               (locate-dominating-file
                default-directory
                (lambda (dir)
                  (thread-last '("config.ru"
                                 "bin/rails"
                                 "config/application.rb"
                                 "config/environment.rb"
                                 "app/controllers/application_controller.rb")
                    (seq-map (lambda (subdir) (expand-file-name subdir dir)))
                    (seq-every-p #'file-regular-p)))))
      (expand-file-name root)))
  '((:test             . "rails test")
    ("run a generator" . (lambda ()
                           (thread-last  "Generate: "
                             read-string
                             (format "rails generate %s")
                             compile)))))

(helm-project-commands-set-build "Rake"
  (lambda ()
    (when-let (root (locate-dominating-file
                     default-directory
                     (lambda (dir)
                       (thread-last '("Rakefile"
                                      "Rakefile.rb"
                                      "rakefile"
                                      "rakefile.rb")
                         (seq-map (lambda (subdir) (expand-file-name subdir dir)))
                         (seq-some #'file-regular-p)))))
      (expand-file-name root)))
  '(("run default task"       . "rake")
    ("specify task(s) to run" . (lambda ()
                                  (thread-last "Taks(s): "
                                    read-string
                                    (format "rake %s")
                                    compile)))))

(helm-project-commands-set-build "Make"
  (lambda ()
    (when-let (root (locate-dominating-file
                     default-directory
                     (lambda (dir)
                       (thread-last '("Makefile"
                                      "makefile"
                                      "GNUmakefile")
                         (seq-map (lambda (subdir) (expand-file-name subdir dir)))
                         (seq-some #'file-regular-p)))))
      (expand-file-name root)))
  '((:build                       . "make")
    ("build specified targets(s)" . (lambda ()
                                      (thread-last "Target(s): "
                                        read-string
                                        (format "make %s")
                                        compile)))))

(helm-project-commands-set-build "Gentoo overlay"
  (lambda ()
    (when-let (root (locate-dominating-file default-directory "profiles/repo_name"))
      (expand-file-name root)))
  '(("scan for QA issues" . "pkgcheck scan")
    ("update manifests"   . "pkgdev manifest")))

(helm-project-commands-set-build "Mix"
  (lambda ()
    (when-let (root (locate-dominating-file default-directory "mix.exs"))
      (expand-file-name root)))
  '((:build                               . "mix compile")
    (:test                                . "mix test")
    ("get dependencies"                   . "mix deps.get")
    ("delete generated application files" . "mix clean")
    ("list outdated dependencies"         . "mix hex.outdated --all")
    ("run dialyzer"                       . "mix dialyzer")
    ("run Elixir formatter"               . "mix format")

    ("run arbitrary mix command" . (lambda ()
                                     (thread-last "Command: "
                                       read-string
                                       (format "mix %s")
                                       compile)))))

(helm-project-commands-set-build "RSpec"
  (lambda ()
    (when-let (root (locate-dominating-file default-directory ".rspec"))
      (expand-file-name root)))
  '((:test . "rspec")))

(helm-project-commands-set-build "Yarn"
  (lambda ()
    (when-let (root (locate-dominating-file default-directory "yarn.lock"))
      (expand-file-name root)))
  '(("install dependencies"       . "yarn --no-emoji --no-progress")
    ("update depencencies"        . "yarn upgrade --no-emoji --no-progress")
    ("list outdated dependencies" . "yarn outdated")

    ("add runtime dependency" . (lambda ()
                                  (thread-last "Package: "
                                    read-string
                                    (format "yarn add --no-emoji --no-progress %s")
                                    compile)))
    ("add development dependency" . (lambda ()
                                      (thread-last "Package: "
                                        read-string
                                        (format "yarn add --dev --no-emoji --no-progress  %s")
                                        compile)))))

(helm-project-commands-set-build "NPM"
  (lambda ()
    (when-let (root (locate-dominating-file default-directory "package.json"))
      (expand-file-name root)))
  '((:test                    . "npm test")
    ("update depencencies"    . "npm update")
    ("list outdated packages" . "npm outdated")
    ("install dependencies"   . "npm install")

    ("add runtime dependency" . (lambda ()
                                  (thread-last "Package: "
                                    read-string
                                    (format "npm install --save %s")
                                    compile)))
    ("add development dependency" . (lambda ()
                                      (thread-last "Package: "
                                        read-string
                                        (format "npm install --save-dev %s")
                                        compile)))))

(provide 'helm-project-commands)
