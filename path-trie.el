;;; -*- lexical-binding: t -*-

(require 'seq)
(require 'map)
(require 'subr-x)

(defun path-trie-split-path (path)
  (seq-map #'intern (split-string path "/" 'omit-nulls)))

(defun path-trie-put-1 (trie path-list obj)
  (let* ((x (car path-list))
         (xs (cdr path-list))
         (current-trie (and (listp trie) trie))
         (sub-trie (and (listp trie) (map-elt trie x)))
         (insert-obj (if xs
                         (path-trie-put-1 sub-trie xs obj)
                       obj)))
    (if sub-trie
        (progn (setf (map-elt current-trie x) insert-obj)
               current-trie)
      (cons (cons x insert-obj) current-trie))))

(defun path-trie-put (trie path obj)
  (unless (file-name-absolute-p path)
    (error "Path must be absolute"))
  (when (listp obj)
    (error "Not allowed to put lists in trie"))
  (path-trie-put-1 trie
                   (path-trie-split-path path)
                   obj))

(defun path-trie-get (trie path)
  (let ((obj (seq-reduce
              (lambda (memo path-item)
                (if (and memo (listp memo))
                    (map-elt memo path-item)
                  memo))
              (path-trie-split-path path)
              trie)))
    (unless (listp obj)
      obj)))

(defun path-trie-remove (trie path)
  (let* ((path-list (path-trie-split-path path))
         (submap (map-nested-elt trie (butlast path-list))))
    (if (and submap (listp submap))
        (prog1
            (path-trie-put-1 trie
                             (butlast path-list)
                             (map-delete submap (car (last path-list))))
          (path-trie-prune-dangling-branch trie path-list))
      trie)))

(defun path-trie-prune-dangling-branch (trie path-list)
  (let ((sub trie)
        (lst path-list))
    (while (not (zerop (path-trie-size (map-elt sub (car lst)))))
      (setq sub (map-elt sub (car lst))
            lst (cdr lst)))
    (map-delete sub (car lst))))

(defun path-trie-reduce (func trie initial-value)
  (seq-reduce
   (lambda (memo key)
     (let ((value (map-elt trie key)))
       (if (listp value)
           (path-trie-reduce func value memo)
         (funcall func memo value))))
   (map-keys trie)
   initial-value))

(defun path-trie-map (mapfunc trie)
  (path-trie-reduce (lambda (memo val)
                      (cons (funcall mapfunc val) memo))
                    trie
                    nil))

(defun path-trie-values (trie)
  (path-trie-map #'identity trie))

(defun path-trie-size (trie)
  (path-trie-reduce (lambda (memo _) (1+ memo)) trie 0))

(provide 'path-trie)
