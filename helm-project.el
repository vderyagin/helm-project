;;; helm-project.el ---  Handling projects in Emacs using Helm  -*- lexical-binding: t -*-

;; Copyright (C) 2015-2021 Victor Deryagin

;; Author: Victor Deryagin <vderyagin@gmail.com>
;; Maintainer: Victor Deryagin <vderyagin@gmail.com>
;; Created: 19 Jun 2015
;; Version: 0.7.4

;; Package-Requires: ((helm) (rg) (magit))

;; This file is not part of GNU Emacs.

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 2, or (at
;; your option) any later version.

;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.

;;; Commentary:

;;; Code:

(require 'helm)
(require 'map)
(require 'path-trie)
(require 'seq)
(require 'subr-x)

(require 'ibuf-ext)
(eval-when-compile (require 'ibuf-macs))

(require 'helm-project-commands)

(defgroup helm-project nil
  "Handling projects in Emacs using Helm"
  :prefix "helm-project-"
  :group 'helm)

(defcustom helm-project-cache-file
  (expand-file-name "helm-project-cache" user-emacs-directory)
  "File to persist projects in"
  :type 'file
  :group 'helm-project)

(defcustom helm-project-readme-default-name "Readme.md"
  "A name for Readme file in project, used when creating a new Readme file"
  :type 'string
  :group 'helm-project)

(defcustom helm-project-readme-wildcard "[Rr][Ee][Aa][Dd][Mm][Ee]*"
  "A wildcard used for detecting Readme files"
  :type 'string
  :group 'helm-project)

(defcustom helm-project-project-predicate (lambda (_) t)
  "Predicate to be called on a project to determine if it's valid.
Can be used to reject some technically-valid projects you don't
want helm-project to handle."
  :type 'function
  :group 'helm-project)

(defvar helm-project-cache nil)
(defvar helm-project-cache-loaded nil)

(defun helm-project-cache-load ()
  (when (and (not helm-project-cache-loaded)
             (file-exists-p helm-project-cache-file))
    (setq helm-project-cache nil)
    (condition-case nil
        (with-temp-buffer
          (insert-file-contents-literally helm-project-cache-file)
          (setq helm-project-cache
                (seq-reduce
                 (pcase-lambda (memo `(,root . ,timestamp))
                   (let ((prj (helm-project-prj :root root
                                                :last-active-at timestamp)))
                     (if (helm-project-prj-valid-p prj)
                         (path-trie-put memo (helm-project-prj-root prj) prj)
                       memo)))
                 (read (current-buffer))
                 nil)
                helm-project-cache-loaded t))
      (error "Failed to read from %s" helm-project-cache-file))))

(defun helm-project-cache-save ()
  (when (or helm-project-cache-loaded (not (file-exists-p helm-project-cache-file)))
    (with-temp-file helm-project-cache-file
      (let ((serialized-cache
             (thread-last helm-project-cache
               path-trie-values
               (seq-map (lambda (prj)
                          (cons (abbreviate-file-name (helm-project-prj-root prj))
                                (helm-project-prj-last-active-at prj))))))
            (print-level nil)
            (print-length nil))
        (prin1 serialized-cache (current-buffer))))))

(defun helm-project-cache-cleanup ()
  (helm-project-cache-load)
  (setq helm-project-cache
        (path-trie-reduce
         (lambda (memo prj)
           (if (helm-project-prj-valid-p prj)
               (path-trie-put memo (helm-project-prj-root prj) prj)
             memo))
         helm-project-cache
         nil)))

(defun helm-project-locate-dominating-file (start name)
  "A version of `locate-dominating-file' that starts at the
filesystem root and moves down the hierarchy closer to START, not
vice-versa."
  (let ((ancestors
         (cl-loop with path = (expand-file-name start)
                  with components = (split-string path "/" 'omit-empty)
                  for i from 1 to (length components)
                  for dir = (concat "/" (string-join (seq-take components i) "/"))
                  if (file-directory-p dir) collect dir))
        (predicate (if (functionp name)
                       name
                     (lambda (dir) (file-exists-p (expand-file-name name dir))))))
    (seq-find predicate ancestors)))

(defun helm-project-rename-file-and-visiting-buffer (file)
  (let ((buf (find-buffer-visiting file))
        (new-name (expand-file-name
                   (read-file-name
                    (format "Rename %s to: " (abbreviate-file-name file))
                    (file-name-directory file)
                    (file-name-base file)))))
    (when new-name
      (when buf
        (with-current-buffer buf
          (save-buffer)))
      (rename-file file new-name)
      (when buf
        (with-current-buffer buf
          (rename-buffer new-name)
          (set-visited-file-name new-name)
          (set-buffer-modified-p nil))))))

(defun helm-project-delete-file-and-visiting-buffer (file)
  (when-let (buf (find-buffer-visiting file))
    (kill-buffer buf))
  (delete-file file))

(defclass helm-project-prj (helm-source)
  ((root
    :initarg :root
    :type string
    :reader helm-project-prj-root)
   (last-active-at
    :initarg :last-active-at
    :initform (float-time)
    :type float
    :reader helm-project-prj-last-active-at)))

(cl-defmethod initialize-instance :after
  ((prj helm-project-prj) &rest _)
  (with-slots (root) prj
    (thread-last root
      expand-file-name
      file-name-as-directory
      (setq root)))
  (helm-project-cache-load)
  (setq helm-project-cache
        (path-trie-put helm-project-cache (helm-project-prj-root prj) prj)))

(cl-defmethod helm-project-prj-touch ((prj helm-project-prj))
  (oset prj last-active-at (float-time)))

(cl-defmethod helm-project-prj-name ((prj helm-project-prj))
  (thread-first prj
    helm-project-prj-root
    directory-file-name
    file-name-nondirectory))

(cl-defmethod helm-project-prj-files ((prj helm-project-prj))
  "Return list of files in project, paths are relative to project root"
  ;; Doing as little work as possible here, to make it fast on large
  ;; repositories. Some more work is done in filtered-candidate-transformer of
  ;; helm source to compensate for this.
  (let ((default-directory (helm-project-prj-root prj)))
    (split-string
     (shell-command-to-string
      "git ls-files -z --cached --others --exclude-standard --full-name")
     "\0"
     'skip-empty)))

(cl-defmethod helm-project-prj-show ((prj helm-project-prj))
  (format "%-50s %s"
          (helm-project-prj-name prj)
          (thread-first prj
            helm-project-prj-root
            directory-file-name
            file-name-directory
            abbreviate-file-name)))

(cl-defmethod helm-project-prj-valid-p ((prj helm-project-prj))
  (and (with-slots (root) prj
         (file-directory-p (expand-file-name ".git" root)))
       (functionp helm-project-project-predicate)
       (funcall helm-project-project-predicate prj)))

(cl-defmethod helm-project-prj-select-file ((prj helm-project-prj))
  (with-slots (root) prj
    (let ((files (helm-project-prj-files prj)))
      (pcase files
        (`nil
         (message "Project contains no files")
         (helm-project-prj-dired prj))
        (`(,single-file)
         (find-file (expand-file-name single-file root)))
        (_
         (helm
          :prompt "File: "
          :buffer " *helm project files*"
          :sources
          (helm-build-sync-source "Files"
            :candidates files
            :candidate-number-limit 100
            :filtered-candidate-transformer
            (lambda (files _source)
              (seq-reverse
               (seq-reduce
                (lambda (memo file)
                  (let ((full-path (expand-file-name file root)))
                    (if (file-exists-p full-path)
                        (cons (cons file full-path)
                              memo)
                      memo)))
                files
                nil)))
            :action '(("Go to file" .
                       find-file)
                      ("Go to file's directory" .
                       (lambda (f)
                         (dired (file-name-directory f))
                         (dired-goto-file f)))
                      ("Rename file" .
                       helm-project-rename-file-and-visiting-buffer)
                      ("Delete file(s)" .
                       (lambda (_)
                         (save-some-buffers)
                         (seq-each
                          #'helm-project-delete-file-and-visiting-buffer
                          (helm-marked-candidates)))))
            :mode-line (list "file(s)" (concat "RET:Select "
                                               "f2:Goto dir "
                                               "f3:Rename "
                                               "f4:Delete "
                                               "M-R:Root "
                                               "M-M:Magit "
                                               "M-G:rg "
                                               "M-E:Eshell "
                                               "M-K:Kill bufs "
                                               "M-O:Moccur "
                                               "M-!:Cmd "
                                               "M-B:Ibuffer "
                                               "M-s:Other project"))
            :keymap (let ((map (make-sparse-keymap)))
                      (set-keymap-parent map helm-map)
                      (define-key map (kbd "M-R")
                        (lambda ()
                          (interactive)
                          (helm-run-after-exit #'helm-project-prj-dired prj)))
                      (define-key map (kbd "M-M")
                        (lambda ()
                          (interactive)
                          (helm-run-after-exit #'helm-project-prj-magit prj)))
                      (define-key map (kbd "M-G")
                        (lambda ()
                          (interactive)
                          (helm-run-after-exit #'helm-project-prj-rg prj)))
                      (define-key map (kbd "M-E")
                        (lambda ()
                          (interactive)
                          (helm-run-after-exit #'helm-project-prj-eshell prj)))
                      (define-key map (kbd "M-K")
                        (lambda ()
                          (interactive)
                          (helm-run-after-exit #'helm-project-prj-kill-buffers prj)))
                      (define-key map (kbd "M-O")
                        (lambda ()
                          (interactive)
                          (helm-run-after-exit #'helm-project-prj-multi-occur prj)))
                      (define-key map (kbd "M-!")
                        (lambda ()
                          (interactive)
                          (helm-run-after-exit #'helm-project-prj-shell-command prj)))
                      (define-key map (kbd "M-s")
                        (lambda ()
                          (interactive)
                          (helm-run-after-exit #'helm-project-select-project)))
                      (define-key map (kbd "C-c C-c")
                        (lambda ()
                          (interactive)
                          (helm-run-after-exit #'helm-project-prj-command prj)))
                      (define-key map (kbd "M-B")
                        (lambda ()
                          (interactive)
                          (helm-run-after-exit #'helm-project-prj-ibuffer prj)))
                      map))))))))

(cl-defmethod helm-project-prj-dired ((prj helm-project-prj))
  (with-slots (root) prj
    (dired root)))

(declare-function magit-status-setup-buffer "magit-status")

(cl-defmethod helm-project-prj-magit ((prj helm-project-prj))
  (require 'magit)
  (with-slots (root) prj
    (magit-status-setup-buffer root)))

(declare-function rg "rg")

(cl-defmethod helm-project-prj-rg ((prj helm-project-prj))
  (with-slots (root) prj
    (rg (read-from-minibuffer "Search for: ") "everything" root)))

(declare-function eshell-kill-input "esh-mode")
(declare-function eshell-send-input "esh-mode")
(declare-function eshell/cd "em-dirs")
(declare-function eshell/pwd "em-dirs")

(cl-defmethod helm-project-prj-eshell ((prj helm-project-prj))
  (require 'eshell)
  (let* ((root (helm-project-prj-root prj))
         (default-directory root)
         (eshell-buffer-name (format "*eshell (%s)*" (abbreviate-file-name root))))
    (call-interactively #'eshell)
    (switch-to-buffer (get-buffer eshell-buffer-name))
    (unless (string= (file-name-as-directory (eshell/pwd)) root)
      (eshell-kill-input)
      (eshell/cd root)
      (eshell-send-input))))

(cl-defmethod helm-project-prj-drop-from-cache ((prj helm-project-prj))
  (with-slots (root) prj
    (setq helm-project-cache (path-trie-remove helm-project-cache root))))

(cl-defmethod helm-project-prj-readme ((prj helm-project-prj))
  (let ((default-directory (helm-project-prj-root prj)))
    (when-let (readmes (file-expand-wildcards helm-project-readme-wildcard
                                              'full-path))
      (car readmes))))

(cl-defmethod helm-project-prj-goto-readme ((prj helm-project-prj))
  (find-file (or (helm-project-prj-readme prj)
                 (expand-file-name helm-project-readme-default-name
                                   (helm-project-prj-root prj)))))

(cl-defmethod helm-project-prj-contains-buffer-p ((prj helm-project-prj) buf)
  (when-let (path (with-current-buffer buf (or buffer-file-name default-directory)))
    (string-prefix-p (helm-project-prj-root prj)
                     (expand-file-name path))))

(cl-defmethod helm-project-prj-buffers ((prj helm-project-prj))
  (seq-filter (lambda (buf)
                (and (not (string-prefix-p " " (buffer-name buf)))
                     (helm-project-prj-contains-buffer-p prj buf)))
              (buffer-list)))

(cl-defmethod helm-project-prj-kill-buffers ((prj helm-project-prj))
  (thread-last prj
    helm-project-prj-buffers
    (seq-each #'kill-buffer)))

(declare-function helm-multi-occur-1 "helm-regexp")

(cl-defmethod helm-project-prj-multi-occur ((prj helm-project-prj))
  (if-let (buffers (seq-filter #'buffer-file-name (helm-project-prj-buffers prj)))
      (progn (require 'helm-regexp)
             (helm-multi-occur-1 buffers))
    (user-error "No files from current project are opened")))

(cl-defmethod helm-project-prj-shell-command ((prj helm-project-prj))
  (let ((default-directory (helm-project-prj-root prj)))
    (shell-command (read-shell-command "Shell command: "))))

(cl-defmethod helm-project-prj-command ((prj helm-project-prj))
  (let ((default-directory
          (if (string-prefix-p (helm-project-prj-root prj)
                               (expand-file-name default-directory))
              default-directory
            (helm-project-prj-root prj))))
    (helm-project-commands-run-command)))

(define-ibuffer-filter helm-project-prj-ibuffer-filter
    "Show Ibuffer with all buffers in current project"
  nil
  (helm-project-prj-contains-buffer-p (thread-last qualifier
                                        expand-file-name
                                        (path-trie-get helm-project-cache))
                                      buf))

(cl-defmethod helm-project-prj-ibuffer ((prj helm-project-prj))
  (ibuffer
   nil
   (format "*Buffers in %s*" (helm-project-prj-name prj))
   (list (cons 'helm-project-prj-ibuffer-filter
               (thread-first prj
                 helm-project-prj-root
                 abbreviate-file-name)))
   nil
   nil
   '(("Directories"
      (predicate . (memq major-mode '(dired-mode wdired-mode))))
     ("Files"
      (predicate . buffer-file-name))
     ("Other buffers"
      (predicate . t)))))

(defun helm-project-select-project ()
  (helm
   :prompt "Project: "
   :buffer " *helm projects*"
   :sources
   (helm-build-sync-source "Projects"
     :init #'helm-project-cache-cleanup
     :candidates (lambda () (path-trie-values helm-project-cache))
     :candidate-transformer (lambda (candidates)
                              (seq-sort
                               (lambda (a b)
                                 (> (helm-project-prj-last-active-at a)
                                    (helm-project-prj-last-active-at b)))
                               candidates))
     :real-to-display #'helm-project-prj-show
     :action '(("Select file"         . helm-project-prj-select-file)
               ("Go to root"          . helm-project-prj-dired)
               ("Magit"               . helm-project-prj-magit)
               ("Search with ripgrep" . helm-project-prj-rg)
               ("Eshell"              . helm-project-prj-eshell)
               ("Go to Readme"        . helm-project-prj-goto-readme)
               ("Kill buffers"        . helm-project-prj-kill-buffers)
               ("Multi-occur"         . helm-project-prj-multi-occur)
               ("Shell command"       . helm-project-prj-shell-command)
               ("Command"             . helm-project-prj-command)
               ("Ibuffer"             . helm-project-prj-ibuffer))
     :action-transformer (lambda (actions _)
                           (seq-map
                            (pcase-lambda (`(,desc . ,action))
                              (cons desc
                                    (lambda (project)
                                      (helm-project-prj-touch project)
                                      (funcall action project))))
                            actions))
     :match-part (lambda (str) (string-trim (substring str 0 50)))
     :mode-line (list "project(s)" (concat "RET:Select a file "
                                           "f2:Go to root "
                                           "f3:Magit "
                                           "f4:rg "
                                           "f5:Eshell "
                                           "f6:Readme "
                                           "f7:Kill bufs "
                                           "f8:Multi-occur "
                                           "f9:Shell cmd "
                                           "f10:cmd "
                                           "f11:Ibuffer "
                                           "C-d:Drop"))
     :keymap (let ((map (make-sparse-keymap)))
               (set-keymap-parent map helm-map)
               (define-key map "/" (kbd "RET"))
               (define-key map (kbd "C-d")
                 (lambda ()
                   (interactive)
                   (seq-each #'helm-project-prj-drop-from-cache (helm-marked-candidates))
                   (helm-refresh)))
               map))))

(defun helm-project-root-detect ()
  "Fetch root of a current project, return `nil' if none."
  (helm-project-locate-dominating-file
   default-directory
   (lambda (dir) (file-directory-p (expand-file-name ".git" dir)))))

(defun helm-project-current-project (&optional notouch)
  "Retrieve current project, either from cache or by looking at file system.
If NOTOUCH artument is non-nil, don't update `last-active-at'
property of project."
  (helm-project-cache-load)
  (or (when-let (cached-project (path-trie-get helm-project-cache default-directory))
        (unless notouch
          (helm-project-prj-touch cached-project))
        cached-project)
      (when-let* ((root (helm-project-root-detect))
                  (prj (helm-project-prj :root root))
                  ((helm-project-prj-valid-p prj)))
        prj)))

(helm-project-commands-set-build "Generic actions"
  (lambda ()
    (when-let (root (helm-project-locate-dominating-file
                     default-directory ".git"))
      (expand-file-name root)))
  (seq-map
   (pcase-lambda (`(,name . ,action))
     (cons name
           (lambda ()
             (when-let (prj (helm-project-current-project))
               (funcall action prj)))))
   '(("Go to project root"                  . helm-project-prj-dired)
     ("Launch Magit"                        . helm-project-prj-magit)
     ("Search with ripgrep"                 . helm-project-prj-rg)
     ("Launch Eshell in project root"       . helm-project-prj-eshell)
     ("Go to Readme"                        . helm-project-prj-goto-readme)
     ("Kill all opened project buffers"     . helm-project-prj-kill-buffers)
     ("Run Multi-occur in project buffers"  . helm-project-prj-multi-occur)
     ("Run a shell command in project root" . helm-project-prj-shell-command)
     ("Open Ibuffer with current project"   . helm-project-prj-ibuffer))))

;;;###autoload
(defun helm-project ()
  (interactive)
  (if-let (prj (helm-project-current-project))
      (helm-project-prj-select-file prj)
    (call-interactively #'helm-project-select)))

;;;###autoload
(defun helm-project-select ()
  (interactive)
  (helm-project-cache-load)
  (if helm-project-cache
      (helm-project-select-project)
    (message "No projects are currently cached, try opening one yourself or run `helm-project-discover'")))

;;;###autoload
(defun helm-project-command ()
  (interactive)
  (if-let (prj (helm-project-current-project))
      (helm-project-prj-command prj)
    (call-interactively #'helm-project-select)))

;;;###autoload
(defun helm-project-discover ()
  (interactive)
  (helm-project-cache-cleanup)

  (cl-labels ((discover-projects-in-dir
               (directory progress-reporter)
               (progress-reporter-update progress-reporter)
               (let ((default-directory directory))
                 (unless (helm-project-current-project 'notouch)
                   (thread-last (directory-files directory 'full-paths "\\`[^.]")
                     (seq-filter #'file-directory-p)
                     (seq-each (lambda (dir) (discover-projects-in-dir dir progress-reporter)))))))
              (strings-common-prefix
               (str1 str2)
               (if (or (member "" (list str1 str2))
                       (not (= (aref str1 0)
                               (aref str2 0))))
                   ""
                 (concat (substring str1 0 1)
                         (strings-common-prefix (substring str1 1)
                                                (substring str2 1))))))
    (let* ((suggested-root (and helm-project-cache
                                (cl-reduce
                                 #'strings-common-prefix
                                 (thread-last helm-project-cache
                                   path-trie-values
                                   (seq-map #'helm-project-prj-root)))))
           (dir (read-directory-name "Directory to discover projects in: "
                                     (expand-file-name suggested-root)
                                     nil
                                     :must-match))
           (progress (make-progress-reporter
                      (format "Discovering projects in %s: "
                              (abbreviate-file-name dir))))
           (project-number-before-discovery (path-trie-size helm-project-cache)))

      (discover-projects-in-dir dir progress)
      (progress-reporter-done progress)

      (when (called-interactively-p 'any)
        (let ((project-number-after-discovery (path-trie-size helm-project-cache)))
          (message "Discovered %d new projects in %s"
                   (- project-number-after-discovery
                      project-number-before-discovery)
                   (abbreviate-file-name dir))))))
  (helm-project-cache-save))

;;;###autoload
(defun helm-project-set-test-command (cmd)
  (interactive "MTest Command: ")
  (if-let (prj (helm-project-current-project))
      (let ((default-directory (helm-project-prj-root prj)))
        (helm-project-commands-set-test-command cmd))
    (user-error "Not in a project")))

;;;###autoload
(defun helm-project-set-build-command (cmd)
  (interactive "MBuild Command: ")
  (if-let (prj (helm-project-current-project))
      (let ((default-directory (helm-project-prj-root prj)))
        (helm-project-commands-set-build-command cmd))
    (user-error "Not in a project")))

(add-hook 'kill-emacs-hook #'helm-project-cache-save)

(provide 'helm-project)

;;; helm-project.el ends here
